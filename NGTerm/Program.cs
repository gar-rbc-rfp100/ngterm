﻿using System;

namespace NGTerm
{
    class Program
    {
        static private Terminal m_terminal;
        static private Protocol m_protocol;

        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args">Arguments passed from the command line.</param>
        static public void Main(string[] args)
        {
            m_protocol = new Protocol();
            m_terminal = new Terminal();

            // Check all of the arguments passed in from the command line
            if (ParseArgs(args) == false)
            {
                PrintUsage();
                PressAnyKey();
                return;
            }

            // Load the XML file that defines the protocol
            if (LoadProtocol() == false)
            {
                PressAnyKey();
                return;
            }

            // Begin a new session (open COM ports)
            if (m_terminal.OpenSession() == false)
            {
                PressAnyKey();
                return;
            }

            // Start a new terminal session (this function will not exit until the user quits)
            m_terminal.RunSession();
            
        }

        /// <summary>
        /// Parse all of the arguments passed in from the command line
        /// </summary>
        /// <param name="args"></param>
        /// <returns>Returns true if arguments could be properly parsed.</returns>
        static private bool ParseArgs(string[] args)
        {
            // Loop through each of the arguments (some will be processed in pairs)
            for (int argIndex = 0; argIndex <= args.GetUpperBound(0); ++argIndex)
            {
                // COM port
                if (args[argIndex] == "-c")
                {

                    if (argIndex >= args.GetUpperBound(0))
                    {
                        System.Console.WriteLine("ERROR: A COM port number is required with the -c option.");
                        System.Console.WriteLine();
                        return false;
                    }

                    try
                    {
                        ++argIndex;
                        m_terminal.PortName = "COM" + Convert.ToInt16(args[argIndex]);
                    }
                    catch
                    {
                        System.Console.WriteLine("ERROR: -c argument must be an integer.");
                        return false;
                    }
                }
                // Baud rate
                else if (args[argIndex] == "-s")
                {
                    if (argIndex >= args.GetUpperBound(0))
                    {
                        System.Console.WriteLine("ERROR: A baud rate is required with the -s option.");
                        System.Console.WriteLine();
                        return false;
                    }

                    try
                    {
                        ++argIndex;
                        m_terminal.BaudRate = Convert.ToInt16(args[argIndex]);
                    }
                    catch
                    {
                        System.Console.WriteLine("ERROR: -s argument must be an integer.");
                        return false;
                    }
                }
                // Debug mode
                else if (args[argIndex] == "-d")
                {
                    m_terminal.DebugMode = true;
                }
                // Protocol filename
                else if (args[argIndex] == "-p")
                {
                    if (argIndex >= args.GetUpperBound(0))
                    {
                        System.Console.WriteLine("ERROR: A protoocol name is required with the -p option.");
                        System.Console.WriteLine();
                        return false;
                    }

                    ++argIndex;
                    m_protocol.Filename = args[argIndex];
                }
                // Timestamp entries
                else if (args[argIndex] == "-t")
                {
                    m_terminal.TimestampMode = true;
                }
                // Log the session
                else if (args[argIndex] == "-l")
                {
                    if (argIndex >= args.GetUpperBound(0))
                    {
                        System.Console.WriteLine("ERROR: A filename is required with the -l option.");
                        System.Console.WriteLine();
                        return false;
                    }
                    ++argIndex;

                    m_terminal.SessionLogMode = true;
                    m_terminal.SessionLogFilename = args[argIndex];
                }
                else if (args[argIndex] == "-rf")
                {
                    if (argIndex >= args.GetUpperBound(0))
                    {
                        System.Console.WriteLine("ERROR: A filename is required with the -rf option.");
                        System.Console.WriteLine();
                        return false;
                    }
                    ++argIndex;

                    m_terminal.RfDataLogMode = true;
                    m_terminal.RfDataLogFilename = args[argIndex];
                }
                // Unknown argument
                else
                {
                    System.Console.WriteLine("ERROR: Unknown argument '" + args[argIndex] + "'.");
                    System.Console.WriteLine();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Loads the XML file that defines the protocol.
        /// </summary>
        /// <returns>Returns true if the XML file could be properly parsed.</returns>
        static bool LoadProtocol()
        {
            try
            {
                XmlParser.ReadXmlFile(m_protocol);
            }
            catch (Exception exc)
            {
                System.Console.WriteLine("ERROR: Unable to open/parse XML Configuration File.");
                System.Console.WriteLine("CAUSE: " + exc.Message);
                return false;
            }

            m_terminal.SessionProtocol = m_protocol;

            return true;
        }

        /// <summary>
        /// Print usage information for this console application.
        /// </summary>
        static private void PrintUsage()
        {
            System.Console.WriteLine("USAGE: ngterm [-c port] [-s speed] [-p protocol] [-d] [-t] [-l filename]");
            System.Console.WriteLine();
            System.Console.WriteLine(" -c port       COM port (default 1)");
            System.Console.WriteLine(" -s speed      Baud rate (default 115200)");
            System.Console.WriteLine(" -d            Display bytes of Tx/Rx messages");
            System.Console.WriteLine(" -p protocol   Name of [protocol].xml configuration file (default rfcip)");
            System.Console.WriteLine("               Available protocols - rfcip, rfcip_safety, rfcip_4_chn, rfcip_test_scale, rfcip_test_syntax");
            System.Console.WriteLine(" -t            Display timestamps of Rx messages");
            System.Console.WriteLine(" -l filename   Log session to [filename]");
            System.Console.WriteLine(" -rf filename  Log all data messages to CSV file [filename]");
        }

        /// <summary>
        /// Waits for the user to press a key to continue. This function was added because if the user starts
        /// the application within Windows Explorer (i.e. double-clicks to start it), the console window
        /// closes too quickly to read any startup error messages.
        /// </summary>
        static private void PressAnyKey()
        {
            System.Console.WriteLine();
            System.Console.WriteLine("<PRESS ANY KEY TO CONTINUE>");

            // Loop until a key is pressed
            while (Console.KeyAvailable == false)
            {
                System.Threading.Thread.Sleep(10);
            }
        }
    }
}
