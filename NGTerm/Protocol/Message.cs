﻿using System;
using System.Collections.Generic;

namespace NGTerm
{
    public enum ParamDataType
    {
        U8,
        U8_ARRAY,
        U16,
        U32,
        S8,
        S16,
        S32,
        STRING
    };

    public struct Param
    {
        public string ParamName;
        public string ParamDesc;
        public ParamDataType DataType;
        public bool Optional;
        public float Scale;
        public string[] EnumValues;
        public Object value;
    }

    public struct Output
    {
        public byte NumParams;
        public string Format;
    }

    public struct Spec
    {
        public byte MessageType;
        public byte MinEntries;
        public byte MaxEntries;
        public List<Output> FormatList;
        public List<Param> ParamList;
    }

    public struct Message
    {
        public string MsgName;
        public string Command;
        public string MsgDesc;
        public Spec Request;
        public Spec Response;
    }
}
