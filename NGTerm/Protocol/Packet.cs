﻿using System;
using System.Collections.Generic;

namespace NGTerm
{
    public class Packet
    {
        public const byte SYNCH_BYTE = 0x55;
        private byte m_numEntries;
        private byte m_bytesPerEntry;
        private byte m_messageType;
        private List<byte[]> m_entryList;
        private byte m_sequence;
        private long m_timestampMs;

        public Packet()
        {
            m_entryList = new List<byte[]>(0);
        }

        public byte NumEntries
        {
            get { return m_numEntries; }
            set { m_numEntries = value; }
        }

        public byte BytesPerEntry
        {
            get { return m_bytesPerEntry; }
            set { m_bytesPerEntry = value; }
        }

        public byte MessageType
        {
            get { return m_messageType; }
            set { m_messageType = value; }
        }

        public byte[] GetEntry(int index)
        {
            if (index < m_entryList.Count)
            {
                return m_entryList[index];
            }

            return null;
        }

        public byte Checksum
        {
            get { return CalculateChecksum(); }
        }

        public byte Sequence
        {
            get { return m_sequence; }
            set { m_sequence = value; }
        }

        public long TimestampMs
        {
            get { return m_timestampMs; }
            set { m_timestampMs = value; }
        }

        public void AddEntry(byte[] byteArray)
        {
            m_entryList.Add(byteArray);

            m_bytesPerEntry = (byte)(byteArray.GetUpperBound(0) + 1);
            ++m_numEntries;
        }

        public byte[] GetEntry(byte entryIndex)
        {
            return m_entryList[entryIndex];
        }

        public byte[] GetMessageByteArray()
        {
            List<Byte> byteList;

            byteList = new List<byte>(256); // Maximum size of message is 4+249+2=261

            byteList.Add(SYNCH_BYTE);
            byteList.Add(m_numEntries);
            byteList.Add(m_bytesPerEntry);
            byteList.Add(m_messageType);
            for (int entryListIndex = 0; entryListIndex < m_numEntries; ++entryListIndex)
            {
                byteList.AddRange(m_entryList[entryListIndex]);
            }
            byteList.Add(CalculateChecksum());
            byteList.Add(m_sequence);

            return byteList.ToArray();
        }

        public string GetMessageByteString()
        {
            string byteString;
            byte[] byteArray;

            byteString = "";
            byteArray = GetMessageByteArray();

            for (int i = 0; i <= byteArray.GetUpperBound(0); ++i)
            {
                byteString += String.Format("{0:x2}", byteArray[i]) + " ";
            }

            byteString = byteString.ToUpper();

            return byteString;
        }

        public byte CalculateChecksum()
        {
            byte checksum = 0;

            checksum += SYNCH_BYTE;
            checksum += m_numEntries;
            checksum += m_bytesPerEntry;
            checksum += m_messageType;

            for (int entryListIndex = 0; entryListIndex < m_numEntries; ++entryListIndex)
            {
                for (int byteIndex = 0; byteIndex <= m_entryList[entryListIndex].GetUpperBound(0); ++byteIndex)
                {
                    checksum += m_entryList[entryListIndex][byteIndex];
                }
            }

            checksum = (byte)((~(checksum)) + 1);

            return checksum;
        }
    }
}
