﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;

namespace NGTerm
{
    /// <summary>
    /// Error that is thrown when a command string could not be parsed according to the specified protocol
    /// such as unknown command, wrong number of arguments, or an invalid value for an argument.
    /// </summary>
    public class ProtocolCommandParserError : System.Exception
    {
        public ProtocolCommandParserError(string message) : base(message) { }
    }

    /// <summary>
    /// Error that is thrown when an incoming packet could not be parsed according to the specified protocol
    /// such as an unexpected number of bytes, invalid message type, or invalid checksum.
    /// </summary>
    public class ProtocolPacketParserError : System.Exception
    {
        public ProtocolPacketParserError(string message) : base(message) { }
    }

    public class Protocol
    {
        private List<Message> m_messageList;
        private string m_revision;
        private string m_filename;
        private string m_description;

        public Protocol()
        {
            m_messageList = new List<Message>();
            m_filename = "rfcip";
        }

        public string Filename
        {
            get { return m_filename; }
            set { m_filename = value; }
        }

        public string Revision
        {
            get { return m_revision; }
            set { m_revision = value; }
        }

        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        public void AddMessage(Message newMessage)
        {
            m_messageList.Add(newMessage);
        }

        public string GetCommandList()
        {
            List<string> commandList = new List<string>(50);
            string allCommands = "";

            foreach (Message nextMessage in m_messageList)
            {
                commandList.Add(" " + nextMessage.Command.PadRight(5) + " " + nextMessage.MsgName);
            }

            commandList.Sort();

            foreach (string command in commandList)
            {
                allCommands += command + "\n";
            }

            return allCommands;
        }

        public string GetCommandUsage(string command)
        {
            Message commandMessage;
            string usage = "";

            commandMessage = CommandToMessage(command);

            usage = "COMMAND: " + command + "\n";
            usage += "NAME: " + commandMessage.MsgName + "\n";
            usage += "PURPOSE: " + commandMessage.MsgDesc + "\n";
            usage += String.Format("REQUEST (0x{0:X2}):\n", commandMessage.Request.MessageType);
            usage += GetParameterUsage(commandMessage.Request);
            usage += String.Format("RESPONSE (0x{0:X2}):\n", commandMessage.Response.MessageType);
            usage += GetParameterUsage(commandMessage.Response);

            return usage;
        }

        public string GetFormatString(Spec formatSpec, byte numParams)
        {
            foreach (Output nextOutput in formatSpec.FormatList)
            {
                if (nextOutput.NumParams == numParams)
                {
                    return nextOutput.Format;
                }
            }

            return "";
        }

        private string GetParameterUsage(Spec usageSpec)
        {
            string usage = "";

            if (usageSpec.MinEntries == usageSpec.MaxEntries)
            {
                usage += "   (Entries: " + usageSpec.MaxEntries + ")\n";
            }
            else
            {

                if (usageSpec.MaxEntries == 0xFF)
                {
                    usage += "   (Entries: " + usageSpec.MinEntries + " to N)\n";
                }
                else
                {
                    usage += "   (Entries: " + usageSpec.MinEntries + " to " + usageSpec.MaxEntries + ")\n";
                }
            }

            if (usageSpec.ParamList.Count > 0)
            {
                foreach (Param usageParam in usageSpec.ParamList)
                {
                    usage += "   " + usageParam.DataType.ToString() + "." + usageParam.ParamName;
                    if (usageParam.Optional == true)
                    {
                        usage += " (Optional)";
                    }
                    if (usageParam.Scale != 1)
                    {
                        usage += " (Scale: " + usageParam.Scale + ")";
                    }
                    usage += ": " + usageParam.ParamDesc + "\n";
                }
            }
            else
            {
                usage += "   (No parameters)\n";
            }

            return usage;
        }

        public Packet CommandToPacket(string command)
        {
            string[] args;
            Message commandMessage;
            Packet commandPacket;
            Spec commandSpec;
            string errorMessage;

            args = command.Split(' ');

            if (args[0].EndsWith("+") == true)
            {
                commandMessage = CommandToMessage(args[0].Substring(0, args[0].Length - 1));
                commandSpec = commandMessage.Response;
            }
            else
            {
                commandMessage = CommandToMessage(args[0]);
                commandSpec = commandMessage.Request;
            }

            for (int argIndex = 0; argIndex <= args.GetUpperBound(0); ++argIndex)
            {
                if (args[argIndex].StartsWith("0X") == true)
                {
                    args[argIndex] = args[argIndex].Substring(2, args[argIndex].Length - 2);
                    args[argIndex] = UInt32.Parse(args[argIndex], System.Globalization.NumberStyles.HexNumber).ToString();
                }
            }

            // NOTE: We want one less than actual number of args, thus GetUpperBound as is. 
            if ((ValidateNumberArguments(args.GetUpperBound(0), commandSpec) == false) &&
                    (commandSpec.ParamList[commandSpec.ParamList.Count - 1].DataType != ParamDataType.U8_ARRAY ))
            {
                errorMessage = "Invalid number of arguments. Enter '? " + commandMessage.Command + "' for usage information.";
                throw new ProtocolCommandParserError(errorMessage);
            }

            commandPacket = new Packet();
            commandPacket.MessageType = commandSpec.MessageType;

            for (int argIndex = 1; argIndex <= args.GetUpperBound(0); )
            {
                List<byte> entry = new List<byte>();

                foreach (Param nextParam in commandSpec.ParamList)
                {
                    if ((argIndex > args.GetUpperBound(0)) && (nextParam.Optional == true))
                    {
                        break;
                    }
                    
                    if( nextParam.DataType == ParamDataType.U8_ARRAY )
                    {
                        while(argIndex < args.Length)
                        {
                            entry.AddRange(CommandArgToByteArray(args[argIndex], nextParam.DataType, nextParam.Scale));
                            ++argIndex;
                        }
                        break;
                    }
                    
                    entry.AddRange(CommandArgToByteArray(args[argIndex], nextParam.DataType, nextParam.Scale));
                    ++argIndex;
                }

                commandPacket.AddEntry(entry.ToArray());
            }

            return commandPacket;
        }

        private Message CommandToMessage(string command)
        {
            string errorMessage;

            foreach (Message findMessage in m_messageList)
            {
                if (command == findMessage.Command)
                {
                    return findMessage;
                }
            }

            errorMessage = "Unknown command: '" + command + "'. Enter '?' for a list of all commands.";
            throw new ProtocolCommandParserError(errorMessage);
        }

        public Packet ByteArrayToPacket(byte[] byteArray)
        {
            Packet byteArrayPacket;
            string error;

            byteArrayPacket = new Packet();

            byteArrayPacket.BytesPerEntry = byteArray[2];
            byteArrayPacket.MessageType = byteArray[3];

            // Not doing byteArray length error checking, but should have already been validated before 
            // reaching here.
            for (int entryIndex = 0; entryIndex < byteArray[1]; ++entryIndex)
            {
                List<byte> entryByteArray = new List<byte>(byteArrayPacket.BytesPerEntry);

                for (int byteIndex = 0; byteIndex < byteArrayPacket.BytesPerEntry; ++byteIndex)
                {
                    entryByteArray.Add(byteArray[4 + (entryIndex * byteArrayPacket.BytesPerEntry) + byteIndex]);
                }

                byteArrayPacket.AddEntry(entryByteArray.ToArray());
            }

            if (byteArrayPacket.Checksum != byteArray[byteArray.GetUpperBound(0) - 1])
            {
                error = String.Format("Received message with invalid checksum. Expected = 0x{0:X2}, Actual = 0x{1:X2}", byteArrayPacket.Checksum, byteArray[byteArray.GetUpperBound(0) - 1]);
                throw new ProtocolPacketParserError(error);
            }

            return byteArrayPacket;
        }

        public Param[] EntryToParamArray(byte[] entryByteArray, byte messageType)
        {
            List<Param> paramList = new List<Param>();
            Spec entrySpec;
            int byteIndex;

            entrySpec = MessageTypeToSpec(messageType);

            byteIndex = 0;
            foreach (Param nextParam in entrySpec.ParamList)
            {
                Param addParam;

                addParam = nextParam;

                if ((nextParam.Optional == true) && (byteIndex > entryByteArray.GetUpperBound(0)))
                {
                    break;
                }

                try
                {
                    switch (nextParam.DataType)
                    {
                        case ParamDataType.U8_ARRAY:
                        case ParamDataType.U8:
                            byte paramU8;
                            paramU8 = entryByteArray[byteIndex];
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramU8 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramU8;
                            }
                            byteIndex += 1;
                            break;
                        case ParamDataType.S8:
                            sbyte paramS8;
                            paramS8 = (sbyte) entryByteArray[byteIndex];
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramS8 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramS8;
                            }
                            byteIndex += 1;
                            break;
                        case ParamDataType.U16:
                            UInt16 paramU16;
                            paramU16 = (UInt16)entryByteArray[byteIndex];
                            paramU16 |= (UInt16)(entryByteArray[byteIndex + 1] << 8);
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramU16 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramU16;
                            }
                            byteIndex += 2;
                            break;
                        case ParamDataType.S16:
                            Int16 paramS16;
                            paramS16 = (Int16)entryByteArray[byteIndex];
                            paramS16 |= (Int16)(entryByteArray[byteIndex + 1] << 8);
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramS16 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramS16;
                            }
                            byteIndex += 2;
                            break;
                        case ParamDataType.U32:
                            UInt32 paramU32;
                            paramU32 = (UInt32)entryByteArray[byteIndex];
                            paramU32 |= (UInt32)(entryByteArray[byteIndex + 1] << 8);
                            paramU32 |= (UInt32)(entryByteArray[byteIndex + 2] << 16);
                            paramU32 |= (UInt32)(entryByteArray[byteIndex + 3] << 24);
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramU32 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramU32;
                            }
                            byteIndex += 4;
                            break;
                        case ParamDataType.S32:
                            Int32 paramS32;
                            paramS32 = (Int32)entryByteArray[byteIndex];
                            paramS32 |= (Int32)(entryByteArray[byteIndex + 1] << 8);
                            paramS32 |= (Int32)(entryByteArray[byteIndex + 2] << 16);
                            paramS32 |= (Int32)(entryByteArray[byteIndex + 3] << 24);
                            if (nextParam.Scale != 1)
                            {
                                addParam.value = paramS32 / nextParam.Scale;
                            }
                            else
                            {
                                addParam.value = paramS32;
                            }
                            byteIndex += 4;
                            break;
                        case ParamDataType.STRING:
                            string paramString = "";
                            List<byte> stringByteList = new List<byte>(0xFF);
                            while (byteIndex <= entryByteArray.GetUpperBound(0))
                            {
                                stringByteList.Add(entryByteArray[byteIndex]);
                                ++byteIndex;
                            }
                            paramString = System.Text.ASCIIEncoding.ASCII.GetString(stringByteList.ToArray());
                            addParam.value = paramString.Trim();
                            break;
                    }
                }
                catch
                {
                    string error;

                    error = "There was an error parsing a byte array for the message type ";
                    error += String.Format("0x{0:X2}", messageType) + ". ";
                    error += "This is likely due to a mismatch between the expected parameter size specified ";
                    error += "in the XML file and the size of the packet received.";

                    throw new ProtocolPacketParserError(error);
                }


                paramList.Add(addParam);
            }

            return paramList.ToArray();
        }

        public Spec MessageTypeToSpec(byte messageType)
        {
            Spec matchingSpec = new Spec();
            string error;
            
            foreach (Message findMessage in m_messageList)
            {
                if (messageType == findMessage.Request.MessageType)
                {
                    matchingSpec = findMessage.Request;
                    break;
                }
                else if (messageType == findMessage.Response.MessageType)
                {
                    matchingSpec = findMessage.Response;
                    break;
                }
            }

            if (matchingSpec.MessageType != messageType)
            {
                error = "Received message with unknown message type 0x" + Terminal.ByteToString(messageType) + ".\n";
                throw new ProtocolPacketParserError(error);
            }

            return matchingSpec;
        }

        public string MessageTypeToString(byte messageType)
        {
            foreach (Message findMessage in m_messageList)
            {
                if (messageType == findMessage.Request.MessageType)
                {
                    return findMessage.MsgName + " Request";
                }
                else if (messageType == findMessage.Response.MessageType)
                {
                    return findMessage.MsgName;
                }
            }

            return "Unknown Message";
        }

        private byte[] CommandArgToByteArray(string arg, ParamDataType argParamDataType, float scale)
        {
            byte[] byteArray;
            double value;

            try
            {
                value = Convert.ToDouble(arg);
            }
            catch (InvalidCastException)
            {
                throw new ProtocolCommandParserError("The argument '" + arg + "' is not a number.\n");
            }

            value = value * scale;

            try
            {
                switch (argParamDataType)
                {
                    case ParamDataType.U8_ARRAY:
                    case ParamDataType.U8:
                        byteArray = new byte[1];
                        byteArray[0] = Convert.ToByte(value);
                        break;
                    case ParamDataType.S8:
                        byteArray = new byte[1];
                        byteArray[0] = (byte)Convert.ToSByte(arg);
                        break;
                    case ParamDataType.U16:
                        UInt16 argU16;
                        argU16 = Convert.ToUInt16(value);
                        byteArray = new byte[2];
                        byteArray[0] = (byte)(argU16);
                        byteArray[1] = (byte)(argU16 >> 8);
                        break;
                    case ParamDataType.S16:
                        Int16 argS16;
                        argS16 = Convert.ToInt16(value);
                        byteArray = new byte[2];
                        byteArray[0] = (byte)(argS16);
                        byteArray[1] = (byte)(argS16 >> 8);
                        break;
                    case ParamDataType.U32:
                        UInt32 argU32;
                        argU32 = Convert.ToUInt32(value);
                        byteArray = new byte[4];
                        byteArray[0] = (byte)(argU32);
                        byteArray[1] = (byte)(argU32 >> 8);
                        byteArray[2] = (byte)(argU32 >> 16);
                        byteArray[3] = (byte)(argU32 >> 24);
                        break;
                    case ParamDataType.S32:
                        Int32 argS32;
                        argS32 = Convert.ToInt32(value);
                        byteArray = new byte[4];
                        byteArray[0] = (byte)(argS32);
                        byteArray[1] = (byte)(argS32 >> 8);
                        byteArray[2] = (byte)(argS32 >> 16);
                        byteArray[3] = (byte)(argS32 >> 24);
                        break;
                    case ParamDataType.STRING:
                        byteArray = new byte[4];
                        break;
                    default:
                        byteArray = new byte[0];
                        break;
                }
            }
            catch (OverflowException)
            {
                throw new ProtocolCommandParserError("The argument '" + arg + "' is outside the range for its data type.\n");
            }

            return byteArray;
        }

        private byte[] SwapBytes(byte[] byteArray)
        {
            byte[] swappedByteArray;
            int byteCount;

            byteCount = (byteArray.GetUpperBound(0) + 1);
            swappedByteArray = new byte[byteCount];

            if (byteCount == 2)
            {
                swappedByteArray[0] = byteArray[1];
                swappedByteArray[1] = byteArray[0];
            }
            else if (byteCount == 4)
            {
                swappedByteArray[0] = byteArray[3];
                swappedByteArray[1] = byteArray[2];
                swappedByteArray[2] = byteArray[1];
                swappedByteArray[3] = byteArray[0];
            }

            return swappedByteArray;
        }

        private bool ValidateNumberArguments(int numArgs, Spec commandSpec)
        {
            if (commandSpec.MinEntries == commandSpec.MaxEntries)
            {
                int minExpectedArgs;
                int maxExpectedArgs;

                maxExpectedArgs = commandSpec.ParamList.Count;

                minExpectedArgs = 0;
                foreach (Param nextParam in commandSpec.ParamList)
                {
                    if (nextParam.Optional == false)
                    {
                        ++minExpectedArgs;
                    }
                }

                if (minExpectedArgs == maxExpectedArgs)
                {
                    if (numArgs != minExpectedArgs)
                    {
                        return false;
                    }
                }
                else
                {
                    if ((numArgs != minExpectedArgs) && (numArgs != maxExpectedArgs))
                    {
                        return false;
                    }
                }
            }
            /* Multiple entries */
            else
            {
                int numExpectedArgs;

                numExpectedArgs = commandSpec.ParamList.Count;

                if ((numExpectedArgs == 0) && (numArgs != 0))
                {
                    return false;
                }
                else if ((numArgs == 0) && (commandSpec.MinEntries != 0))
                {
                    return false;
                }
                else if ((numArgs % numExpectedArgs) != 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
