﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace NGTerm
{
    public class ProtocolXmlParserError : System.Exception
    {
        public ProtocolXmlParserError(string message) : base(message) { }
    }

    public class XmlParser
    {
        static Protocol m_protocol;

        static public void ReadXmlFile(Protocol newProtocol)
        {
            XmlDocument protocolXmlDocument = new XmlDocument();
            XmlNode protocolXmlNode;
            XmlNodeList protocolXmlNodeList;
            string message;

            m_protocol = newProtocol;

            // default name is rfcip, if none entered 
            if (m_protocol.Filename == "rfcip")
            {
                protocolXmlDocument.LoadXml(NGTerm.Properties.Resources.rfcip);
            }
            else if (m_protocol.Filename == "rfcip_safety")
            {
                protocolXmlDocument.LoadXml(NGTerm.Properties.Resources.rfcip_safety);
            }
            else if (m_protocol.Filename == "rfcip_4_chn")
            {
                protocolXmlDocument.LoadXml(NGTerm.Properties.Resources.rfcip_4_chn);
            }
            else if (m_protocol.Filename == "rfcip_test_scale")
            {
                protocolXmlDocument.LoadXml(NGTerm.Properties.Resources.rfcip_test_scale);
            }
            else if (m_protocol.Filename == "rfcip_test_syntax")
            {
                protocolXmlDocument.LoadXml(NGTerm.Properties.Resources.rfcip_test_syntax);
            }
            else
            {
                message = "Invalid Protocol Specification XML file.\n";
                throw new ProtocolXmlParserError(message);
            }                        

            protocolXmlNode = protocolXmlDocument.SelectSingleNode("Protocol");
            protocolXmlNodeList = protocolXmlNode.ChildNodes;

            foreach (XmlNode protocolChildXmlNode in protocolXmlNodeList)
            {
                if (protocolChildXmlNode.NodeType == XmlNodeType.Comment)
                {
                    continue;
                }
                else if (protocolChildXmlNode.Name == "ProtocolDesc")
                {
                    m_protocol.Description = protocolChildXmlNode.InnerText;
                }
                else if (protocolChildXmlNode.Name == "ProtocolRev")
                {
                    m_protocol.Revision = protocolChildXmlNode.InnerText;
                }
                else if (protocolChildXmlNode.Name == "Message")
                {
                    m_protocol.AddMessage(ReadMessage(protocolChildXmlNode));
                }
                else
                {
                    message = "Invalid child node of <Protocol> node in Protocol Specification XML file.\n";
                    message += "Invalid Node: <" + protocolChildXmlNode.Name + ">\n";
                    throw new ProtocolXmlParserError(message);
                }
            }

            if (m_protocol.Description == null)
            {
                message = "The node is <ProtocolDesc> is not defined in the XML file.";
                throw new ProtocolXmlParserError(message);
            }
            else if (m_protocol.Revision == null)
            {
                message = "The node is <ProtocolRev> is not defined in the XML file.";
                throw new ProtocolXmlParserError(message);
            }
        }

        static private Message ReadMessage(XmlNode messageXmlNode)
        {
            XmlNodeList messageXmlChildNodeList;
            Message newMessage = new Message();
            string error;

            messageXmlChildNodeList = messageXmlNode.ChildNodes;

            foreach (XmlNode messageXmlChildNode in messageXmlChildNodeList)
            {
                if (messageXmlChildNode.NodeType == XmlNodeType.Comment)
                {
                    continue;
                }
                else if (messageXmlChildNode.Name == "MsgName")
                {
                    newMessage.MsgName = messageXmlChildNode.InnerText;
                }
                else if (messageXmlChildNode.Name == "Command")
                {
                    newMessage.Command = messageXmlChildNode.InnerText;
                }
                else if (messageXmlChildNode.Name == "MsgDesc")
                {
                    newMessage.MsgDesc = messageXmlChildNode.InnerText;
                }
                else if (messageXmlChildNode.Name == "Request")
                {
                    newMessage.Request = ReadSpec(messageXmlChildNode, newMessage);
                }
                else if (messageXmlChildNode.Name == "Response")
                {
                    newMessage.Response = ReadSpec(messageXmlChildNode, newMessage);
                }
                else
                {
                    error = "Invalid child node of <Message> node in Protocol Specification XML file.\n";
                    error += "Parent Message Type: " + newMessage.MsgName + "\n";
                    error += "Node Name: <" + messageXmlChildNode.Name + ">";
                    throw new ProtocolXmlParserError(error);
                }
            }

            return newMessage;
        }

        static private Spec ReadSpec(XmlNode dataXmlNode, Message newMessage)
        {
            Spec newSpec = new Spec();
            XmlNodeList dataXmlChildNodeList;
            string error;

            dataXmlChildNodeList = dataXmlNode.ChildNodes;

            newSpec.MinEntries = Byte.MinValue;
            newSpec.MaxEntries = Byte.MaxValue;
            newSpec.FormatList = new List<Output>();
            newSpec.ParamList = new List<Param>();

            foreach (XmlNode dataXmlChildNode in dataXmlChildNodeList)
            {
                if (dataXmlChildNode.NodeType == XmlNodeType.Comment)
                {
                    continue;
                }
                else if (dataXmlChildNode.Name == "Type")
                {
                    string hexNumber;

                    hexNumber = dataXmlChildNode.InnerText;
                    if (hexNumber.StartsWith("0x") == true)
                    {
                        hexNumber = hexNumber.Substring(2, hexNumber.Length - 2);
                    }
                    newSpec.MessageType = Byte.Parse(hexNumber, System.Globalization.NumberStyles.HexNumber);

                }
                else if (dataXmlChildNode.Name == "MinEntries")
                {
                    newSpec.MinEntries = Convert.ToByte(dataXmlChildNode.InnerText);
                }
                else if (dataXmlChildNode.Name == "MaxEntries")
                {
                    newSpec.MaxEntries = Convert.ToByte(dataXmlChildNode.InnerText);
                }
                else if (dataXmlChildNode.Name == "Format")
                {
                    newSpec.FormatList.Add(ReadFormat(dataXmlChildNode));
                }
                else if (dataXmlChildNode.Name == "Entry")
                {
                    XmlNodeList entryXmlChildNodeList;

                    entryXmlChildNodeList = dataXmlChildNode.ChildNodes;

                    foreach (XmlNode paramXmlNode in entryXmlChildNodeList)
                    {
                        if (paramXmlNode.NodeType == XmlNodeType.Comment)
                        {
                            continue;
                        }
                        else if (paramXmlNode.Name == "Param")
                        {
                            newSpec.ParamList.Add(ReadParam(paramXmlNode));
                        }
                        else if (paramXmlNode.Name == "SameAsRequest")
                        {
                            if (Convert.ToBoolean(paramXmlNode.InnerText) == true)
                            {
                                (newSpec.ParamList).AddRange(newMessage.Request.ParamList);
                            }
                        }
                        else
                        {
                            error = "Invalid child node of <Entry> node in Protocol Specification XML file.\n";
                            error += "Parent Message Type: " + newMessage.MsgName + "\n";
                            error += "Node Name: <" + dataXmlChildNode.Name + ">";
                            throw new ProtocolXmlParserError(error);
                        }
                    }
                }
                else
                {
                    error = "Invalid child node of <Message> node in Protocol Specification XML file.\n";
                    error += "Parent Message Type: " + newMessage.MsgName + "\n";
                    error += "Node Name: <" + dataXmlChildNode.Name + ">\n";
                    throw new ProtocolXmlParserError(error);
                }
            }

            // If no parameters are defined, we can assume that MaxEntries should 0.
            if (newSpec.ParamList.Count == 0)
            {
                newSpec.MaxEntries = 0;
            }

            return newSpec;
        }

        static private Param ReadParam(XmlNode paramXmlNode)
        {
            Param param = new Param();
            XmlNodeList paramXmlChildNodeList;
            string message;

            paramXmlChildNodeList = paramXmlNode.ChildNodes;

            param.Scale = 1;

            foreach (XmlNode paramXmlChildNode in paramXmlChildNodeList)
            {
                if (paramXmlChildNode.NodeType == XmlNodeType.Comment)
                {
                    continue;
                }
                else if (paramXmlChildNode.Name == "ParamName")
                {
                    param.ParamName = paramXmlChildNode.InnerText;
                }
                else if (paramXmlChildNode.Name == "ParamDesc")
                {
                    param.ParamDesc = paramXmlChildNode.InnerText;
                }
                else if (paramXmlChildNode.Name == "DataType")
                {
                    param.DataType = (ParamDataType)Enum.Parse(typeof(ParamDataType), paramXmlChildNode.InnerText, true);
                }
                else if (paramXmlChildNode.Name == "Optional")
                {
                    param.Optional = Convert.ToBoolean(paramXmlChildNode.InnerText);
                }
                else if (paramXmlChildNode.Name == "Scale")
                {
                    param.Scale = Convert.ToInt32(paramXmlChildNode.InnerText);
                }
                else if (paramXmlChildNode.Name == "EnumValues")
                {
                    param.EnumValues = (paramXmlChildNode.InnerText).Split(',');

                    for (int i = 0; i <= (param.EnumValues).GetUpperBound(0); ++i)
                    {
                        param.EnumValues[i] = (param.EnumValues[i]).Trim();
                    }
                }
                else
                {
                    message = "Invalid child node of <Param> node in Protocol Specification XML file.\n";
                    message += "Node Name: <" + paramXmlChildNode.Name + ">";
                    throw new ProtocolXmlParserError(message);
                }
            }

            return param;
        }

        static private Output ReadFormat(XmlNode formatXmlNode)
        {
            Output newOutput = new Output();
            string message;
            string formatString;
            byte openBracketCount = 0;
            byte closeBracketCount = 0;

            formatString = formatXmlNode.InnerText;

            foreach (char formatChar in formatString.ToCharArray())
            {
                if (formatChar == '{')
                {
                    ++openBracketCount;
                }
                else if (formatChar == '}')
                {
                    ++closeBracketCount;
                }
            }

            if (openBracketCount != closeBracketCount)
            {
                message = "Malformed format string, " + formatString;
                throw new ProtocolXmlParserError(message);
            }

            newOutput.NumParams = openBracketCount;
            newOutput.Format = formatString;

            return newOutput;
        }
    }
}
