﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports;
using System.IO;
using System.Diagnostics;

namespace NGTerm
{
    public class Terminal
    {
        private const int MSG_HEADER_LEN = 4;
        private const int MSG_FOOTER_LEN = 2;
        private const byte DATA_MESSAGE = 0x88;
        private const string PROMPT = "$";

        private List<string> m_commandHistoryList;
        private int m_selectedCommand = 0;

        private Protocol m_protocol;

        private SerialPort m_serialPort;

        private Queue<Packet> m_rxPacketQueue;
        private List<byte> m_currentRxByteList;

        private Queue<string> m_scriptCommandQueue;

        private bool m_debugMode = false;
        private bool m_wideMode = true;
        private bool m_timestampMode = false;
        private bool m_sessionLogMode = false;
        private bool m_rfDataLogMode = false;

        private StreamWriter m_sessionLogStreamWriter;
        private StreamWriter m_rfDataLogStreamWriter;
        private bool m_rfDataLogHeaderPrinted = false;

        private bool m_pauseForInput = false;
        private bool m_scriptRunning = false;

        private long m_scriptWaitTime = 0;
        private long m_scriptStartTime = 0;

        private string m_currentCommand = "";

        private Stopwatch m_stopwatch;

        private byte m_sequence = 0;

        /// <summary>
        /// Default constructor for a new terminal. Defaults to COM1 and baud rate of 1152000
        /// </summary>
        public Terminal()
        {
            // Create a new list of commands
            m_commandHistoryList = new List<string>(100);

            m_scriptCommandQueue = new Queue<string>(20);

            m_currentRxByteList = new List<byte>(0xFF);
            m_rxPacketQueue = new Queue<Packet>(100);

            m_serialPort = new SerialPort();
            m_serialPort.BaudRate = 115200;
            m_serialPort.PortName = "COM1";
            m_serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(SerialPort_DataReceived);

            Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

            m_stopwatch = new Stopwatch();
        }

        public int BaudRate
        {
            get { return m_serialPort.BaudRate; }
            set { m_serialPort.BaudRate = value; }
        }

        public string PortName
        {
            get { return m_serialPort.PortName; }
            set { m_serialPort.PortName = value; }
        }

        public bool DebugMode
        {
            get { return m_debugMode; }
            set { m_debugMode = value; }
        }

        public bool TimestampMode
        {
            get { return m_timestampMode; }
            set { m_timestampMode = value; }
        }

        public bool SessionLogMode
        {
            get { return m_sessionLogMode; }
            set { m_sessionLogMode = value; }
        }

        public bool RfDataLogMode
        {
            get { return m_rfDataLogMode; }
            set { m_rfDataLogMode = value; }
        }

        public Protocol SessionProtocol
        {
            get { return m_protocol; }
            set { m_protocol = value; }
        }


        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            FlushLogBuffers();
        }

        private void FlushLogBuffers()
        {
            if (m_sessionLogMode == true)
            {
                m_sessionLogStreamWriter.Flush();
            }

            if (m_rfDataLogMode == true)
            {
                m_rfDataLogStreamWriter.Flush();
            }
        }

        /// <summary>
        /// The session log filename
        /// </summary>
        public string SessionLogFilename
        {
            set
            {
                try
                {
                    m_sessionLogStreamWriter = new StreamWriter(value, false);
                }
                // Catch any exception when opening the file and display what the problem was
                catch (Exception exc)
                {
                    string error;

                    error = "ERROR: Could not open session log file, '" + value + "'\n";
                    error += "CAUSE: " + exc.Message + "\n";
                    TerminalOut(error);

                    m_sessionLogMode = false;
                }
            }
        }

        /// <summary>
        /// The RF data log filename
        /// </summary>
        public string RfDataLogFilename
        {
            set
            {
                try
                {
                    m_rfDataLogStreamWriter = new StreamWriter(value, false);
                }
                // Catch any exception when opening the file and display what the problem was
                catch (Exception exc)
                {
                    string error;

                    error = "ERROR: Could not open RF data log file, '" + value + "'\n";
                    error += "CAUSE: " + exc.Message + "\n";
                    TerminalOut(error);

                    m_rfDataLogMode = false;
                }               
            }

        }

        /// <summary>
        /// Function which handles incoming bytes received over the serial port
        /// </summary>
        /// <param name="sender">The object (COM port) receving the bytes</param>
        /// <param name="e">Event associated with the data receive event</param>
        private void SerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte rxByte;

            // Loop as long as there are moreo bytes to receive
            while (m_serialPort.BytesToRead > 0)
            {
                // Read the current byte in the input queue
                rxByte = (byte)m_serialPort.ReadByte();

                // If we are waiting for the first byte of a new message, and the current
                // byte is not the synch byte (signifying the start of a message), discard
                // the byte and continue looking for a synch byte
                if ((m_currentRxByteList.Count == 0) && (rxByte != Packet.SYNCH_BYTE))
                {
                    continue;
                }

                // Add the byte to the current receive packet
                m_currentRxByteList.Add(rxByte);

                // If we have at least the first 3 bytes, then we should be able to determine the
                // length of the message
                if (m_currentRxByteList.Count >= 3)
                {
                    int expectedLength;
                    byte numEntries;
                    byte bytesPerEntry;

                    // The number of entries and bytes per entry are specified in the 2nd and 3rd bytes respectively
                    numEntries = m_currentRxByteList[1];
                    bytesPerEntry = m_currentRxByteList[2];
                   
                    // Determine the length of the message based on the number of entries
                    if (numEntries == 0)
                    {
                        // If the number of entries is zero, then there is no payload and the message length
                        // is equal to the length of the header and footer
                        expectedLength = MSG_HEADER_LEN + MSG_FOOTER_LEN;
                    }
                    else
                    {
                        // If the number of entries is non-zero, then the message has a payload
                        expectedLength = MSG_HEADER_LEN + (numEntries * bytesPerEntry) + MSG_FOOTER_LEN;
                    }

                    // If we've gotten enough bytes for a complete message, send it off for processing
                    if (m_currentRxByteList.Count >= expectedLength)
                    {
                        Packet rxPacket;

                        try
                        {
                            // Pack the byte array into a Packet structure
                            rxPacket = m_protocol.ByteArrayToPacket(m_currentRxByteList.ToArray());
                        }
                        catch (ProtocolPacketParserError exc)
                        {
                            TerminalOut("ERROR: Could not process incoming message\n");
                            TerminalOut("CAUSE: " + exc.Message + "\n");
                            TerminalOut("MESSAGE: ");
                            foreach (byte nextByte in m_currentRxByteList)
                            {
                                TerminalOut(ByteToString(nextByte) + " ");
                            }
                            TerminalOut("\n");

                            return;
                        }
                        finally
                        {
                            // Reset the "current" byte list to be ready for next meessage
                            m_currentRxByteList.Clear();
                        }

                        // Set the timestamp for the time received
                        rxPacket.TimestampMs = m_stopwatch.ElapsedMilliseconds;

                        // Add the new packet to a queue for later processing
                        m_rxPacketQueue.Enqueue(rxPacket);
                    }
                }
            }

        }

        /// <summary>
        /// Open a new 
        /// </summary>
        /// <returns></returns>
        public bool OpenSession()
        {
            Version ngtermVersion;

            try
            {
                m_serialPort.Open();
            }
            catch (Exception exc)
            {
                System.Console.WriteLine("ERROR: Unable to start terminal session.");
                System.Console.WriteLine("CAUSE: " + exc.Message);
                return false;
            }

            // Set the console window to 120 columns
            if (m_wideMode == true)
            {
                Console.WindowWidth = 120;
            }

            // Display the version of this assembly
            ngtermVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            TerminalOut("NGTERM Version " + ngtermVersion.Major + "." + ngtermVersion.Minor + "." + ngtermVersion.Build + "\n");

            // Display the version of the XML configuration file (as recorded in the file itself)
            TerminalOut("Protocol: " + m_protocol.Description + " (Rev " + m_protocol.Revision + ")" + "\n");

            // Start the stopwatch used for timestamping incoming messages
            m_stopwatch.Start();

            return true;
        }

        public void RunSession()
        {
            /* Terminal superloop */
            while (true)
            {
                Thread.Sleep(10);
                if (m_pauseForInput == false)
                {
                    try
                    {
                        ProcessMessageQueue();
                    }
                    catch (ProtocolPacketParserError exc)
                    {
                        TerminalOut("ERROR: There was an error processing an incoming packet.\n");
                        TerminalOut("CAUSE: " + exc.Message + "\n");
                    }
                    catch (Exception exc)
                    {
                        TerminalOut("ERROR: There was an uncaught exception while processing incoming data packet.\n");
                        TerminalOut("MESSAGE: " + exc.Message + "\n");
                    }
                }

                try
                {
                    if (m_scriptRunning == false)
                    {
                        ProcessInput();
                    }
                    else
                    {
                        if (m_stopwatch.ElapsedMilliseconds > (m_scriptStartTime + m_scriptWaitTime))
                        {
                            if (m_scriptCommandQueue.Count != 0)
                            {
                                m_currentCommand = m_scriptCommandQueue.Dequeue();
                                m_scriptStartTime = m_stopwatch.ElapsedMilliseconds;
                                m_scriptWaitTime = 100;

                                TerminalOut(PROMPT + m_currentCommand + "\n");

                                if (m_currentCommand.StartsWith("."))
                                {
                                    ProcessSpecialCommand();
                                }
                                else
                                {
                                    ProcessCommand();
                                }

                                m_currentCommand = "";
                            }
                            else
                            {
                                m_scriptRunning = false;
                            }
                        }

                    }
                }
                catch (Exception exc)
                {
                    TerminalOut("ERROR: There was an uncaught exception while processing a command.\n");
                    TerminalOut("MESSAGE: " + exc.Message + "\n");
                }
            }
        }

        private void ProcessMessageQueue()
        {
            if (m_rxPacketQueue.Count > 0)
            {
                Packet rxPacket = m_rxPacketQueue.Dequeue();

                if (m_debugMode == true)
                {
                    PrintDebugPacket(rxPacket);
                }

                PrintFormattedPacket(rxPacket);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rxPacket"></param>
        private void PrintFormattedPacket(Packet rxPacket)
        {
            Spec packetSpec;

            packetSpec = m_protocol.MessageTypeToSpec(rxPacket.MessageType);

            if (rxPacket.NumEntries == 0)
            {
                TerminalOut(m_protocol.MessageTypeToString(rxPacket.MessageType) + "\n");
            }

            for (int entryIndex = 0; entryIndex < rxPacket.NumEntries; ++entryIndex)
            {
                Param[] paramArray;
                string formatString;

                paramArray = m_protocol.EntryToParamArray(rxPacket.GetEntry(entryIndex), rxPacket.MessageType); 
                formatString = m_protocol.GetFormatString(packetSpec, (byte) (paramArray.GetUpperBound(0) + 1));

                if (m_timestampMode == true)
                {
                    TerminalOut(String.Format("{0:00000000} ", rxPacket.TimestampMs));                    
                }

                TerminalOut(m_protocol.MessageTypeToString(rxPacket.MessageType));

                if (paramArray.GetUpperBound(0) >= 0)
                {
                    TerminalOut(": ");

                    if (formatString == "")
                    {
                        foreach (Param nextParam in paramArray)
                        {
                            TerminalOut(nextParam.ParamName + "=" + nextParam.value + " ");
                        }
                        TerminalOut("\n");
                    }
                    else
                    {
                        List<Object> objList;

                        objList = new List<Object>(paramArray.GetUpperBound(0) + 1);

                        foreach (Param nextParam in paramArray)
                        {
                            objList.Add(nextParam.value);
                        }

                        try
                        {
                            TerminalOut(String.Format(formatString, objList.ToArray()) + "\n");
                        }
                        catch (FormatException)
                        {
                            string message = "There was an error formatting the output string. Inspect the <Format> node for the message type in the XML configuration file ";
                            message += "to make sure that the parameters in the format string matches the expected number of parameters and data type for the message type.";
                            throw new ProtocolPacketParserError(message);
                        }
                    }
                }

                if ((m_rfDataLogMode == true) && (rxPacket.MessageType == DATA_MESSAGE))
                {
                    List<string> rfDataList;

                    rfDataList = new List<string>(paramArray.GetUpperBound(0) + 1);

                    if (m_rfDataLogHeaderPrinted == false)
                    {
                        string header;

                        header = "Timestamp (ms)";

                        foreach (Param rfParam in packetSpec.ParamList)
                        {
                            header += "," + rfParam.ParamName;
                        }

                        m_rfDataLogStreamWriter.WriteLine(header);

                        m_rfDataLogHeaderPrinted = true;
                    }

                    foreach (Param nextParam in paramArray)
                    {
                        rfDataList.Add(nextParam.value.ToString());
                    }

                    m_rfDataLogStreamWriter.Write(String.Format("{0:00000000} ", rxPacket.TimestampMs) + ",");
                    m_rfDataLogStreamWriter.WriteLine(String.Join(",", rfDataList.ToArray()));
                }
            }
        }

        private void ProcessInput()
        {
            ConsoleKeyInfo keyConsole;
            char keyChar;

            if (Console.KeyAvailable == true)
            {
                if (m_pauseForInput == false)
                {
                    Console.Write(PROMPT);
                }

                m_pauseForInput = true;

                keyConsole = Console.ReadKey(true);
                keyChar = Char.ToUpper(keyConsole.KeyChar);

                if (Char.IsLetterOrDigit(keyChar) || (keyChar == ' ') || (keyChar == '-') || (keyChar == ',') || (keyChar == '?') || (keyChar == '.') || (keyChar == '+'))
                {
                    Console.Write(keyChar.ToString());
                    m_currentCommand += keyChar;
                }
                else if (keyConsole.Key == ConsoleKey.Backspace)
                {
                    if (m_currentCommand.Length > 0)
                    {
                        Console.Write(keyConsole.KeyChar.ToString());
                        Console.Write(" ");
                        Console.Write(keyConsole.KeyChar.ToString());
                        m_currentCommand = m_currentCommand.Remove(m_currentCommand.Length - 1);
                    }
                }
                else if ((keyConsole.Key == ConsoleKey.UpArrow) || (keyConsole.Key == ConsoleKey.DownArrow))
                {
                    if (m_currentCommand.Length > 0)
                    {
                        Console.Write("\r");
                        Console.Write(new String(' ', m_currentCommand.Length + PROMPT.Length));
                        Console.Write("\r");
                        Console.Write(PROMPT);
                    }

                    if (keyConsole.Key == ConsoleKey.UpArrow)
                    {
                        if (m_selectedCommand < m_commandHistoryList.Count)
                        {
                            ++m_selectedCommand;
                        }

                        if ((m_commandHistoryList.Count - m_selectedCommand) < m_commandHistoryList.Count)
                        {
                            m_currentCommand = m_commandHistoryList[m_commandHistoryList.Count - m_selectedCommand];
                            Console.Write(m_currentCommand);
                        }
                    }
                    else
                    {
                        if (m_selectedCommand > 1)
                        {
                            --m_selectedCommand;
                            m_currentCommand = m_commandHistoryList[m_commandHistoryList.Count - m_selectedCommand];
                            Console.Write(m_currentCommand);
                        }
                        else
                        {
                            m_selectedCommand = 0;
                            m_currentCommand = "";
                        }
                    }
                }
                else if (keyConsole.Key == ConsoleKey.Escape)
                {
                    FlushLogBuffers();
                    System.Environment.Exit(0);                   
                }
                else if (keyConsole.Key == ConsoleKey.Enter)
                {
                    Console.Write("\n");

                    m_currentCommand = m_currentCommand.Trim();

                    if (m_sessionLogMode == true)
                    {
                        LogOut(PROMPT + m_currentCommand + "\n");
                    }

                    m_commandHistoryList.Add(m_currentCommand);
                    m_selectedCommand = 0;
                    while (m_commandHistoryList.Count > 100)
                    {
                        m_commandHistoryList.RemoveAt(0);
                    }

                    if (m_currentCommand.StartsWith(".") || m_currentCommand.StartsWith("?"))
                    {
                        ProcessSpecialCommand();
                    }
                    else
                    {
                        ProcessCommand();
                    }

                    m_pauseForInput = false;
                    m_currentCommand = "";
                }
            }
        }

        private void ProcessSpecialCommand()
        {
            // TODO: Remove. For testing only.
            if (m_currentCommand == ".TEST")
            {
                //byte[] rxMessage = { 0x55, 0x01, 0x08, 0x84, 0x01, 0x04, 0x00, 0x01, 0x01, 0x04, 0x00, 0x04, 0x0F, 0x0A };
                //byte[] rxMessage = { 0x55, 0x01, 0x17, 0x8A, 0xA9, 0x7C, 0x00, 0x1E, 0x49, 0x52, 0x51, 0x5F, 0x53, 0x54, 0x41, 0x43, 0x4B, 0x5F, 0x4F, 0x56, 0x45, 0x52, 0x46, 0x4C, 0x4F, 0x57, 0x00, 0x32, 0x00 };
                //byte[] rxMessage = { 0x55, 0x00, 0x00, 0x1F, 0x8C, 0x01 };
                //byte[] rxMessage = { 0x55, 0x01, 0x04, 0x95, 0xD9, 0x2E, 0xA9, 0x00, 0x61, 0x00 };
                byte[] rxMessage = { 0x55, 0x01, 0x0A, 0xB6, 0x14, 0x00, 0x64, 0x00, 0x00, 0x00, 0x20, 0x02, 0x03, 0x00, 0x4D, 0x00 };


                Packet rxPacket;

                rxPacket = m_protocol.ByteArrayToPacket(rxMessage);
                m_rxPacketQueue.Enqueue(rxPacket);

                return;
            }

            if (m_currentCommand.StartsWith(".RUN") == true)
            {
                RunScript();
            }
            else if (m_currentCommand.StartsWith(".WAIT") == true)
            {
                SetWaitPeriod();
            }
            else if (m_currentCommand.StartsWith(".DEBUG") == true)
            {
                SetDebugMode();
            }
            else if (m_currentCommand.StartsWith(".TIME") == true)
            {
                SetTimestampMode();
            }
            else if (m_currentCommand.StartsWith(".LOG") == true)
            {
                SetLogMode();
            }
            else if (m_currentCommand.StartsWith(".RFDATA ") == true)
            {
                SetRFDataMode();
            }
            else if (m_currentCommand.StartsWith(".COMMENT") == true)
            {
            }
            else if (m_currentCommand == "?")
            {
                DisplayCommandList();
            }
            else if (m_currentCommand.StartsWith("? ") == true)
            {
                string command = m_currentCommand.Substring(2, m_currentCommand.Length - 2);
                DisplayCommandUsage(command);
            }
            else if (m_currentCommand == "??")
            {
                DisplaySpecialCommands();
            }
            else
            {
                string[] args = m_currentCommand.Split(' ');
                TerminalOut("ERROR: Unknown command '" + args[0] + "'" + "\n");
            }
        }

        private void SetWaitPeriod()
        {
            string[] args;
            long waitTimeMs;

            args = m_currentCommand.Split(' ');

            // Check that the command contains only two tokens: [.RUN] [filename]
            if (args.GetUpperBound(0) != 1)
            {
                TerminalOut("ERROR: The .WAIT command takes a single argument, the time to wait in milliseconds.\n");
                return;
            }

            try
            {
                waitTimeMs = Convert.ToInt64(args[1]);
            }
            catch
            {
                TerminalOut("ERROR: The .WAIT command expects a number as an argument, the time to wait in milliseconds.\n");
                return;
            }

            m_scriptWaitTime = waitTimeMs;
        }

        private void DisplaySpecialCommands()
        {
            TerminalOut("Special Commands:\n");
            TerminalOut(".RUN [filename]            Run the script with the specified filename\n");
            TerminalOut(".WAIT [ms]                 Wait the specified amount of milliseconds before proceeding to the next command\n");
            TerminalOut(".DEBUG [OFF|ON]            Disable or enable debug information\n");
            TerminalOut(".TIME [OFF|ON]             Disable or enable timestamps on incoming messages\n");
            TerminalOut(".LOG [OFF|ON filename]     Disable or enable session logging\n");
            TerminalOut(".RFDATA [OFF|ON filename]  Disable or enable RF data logging\n");
            TerminalOut(".COMMENT [information]     Does nothing. Allows comments inside scripts\n");
        }

        private void SetDebugMode()
        {
            string[] args;

            args = m_currentCommand.Split(' ');

            if (args.GetUpperBound(0) != 1)
            {
                TerminalOut("ERROR: The .DEBUG command takes a single argument, ON or OFF.\n");
                return;
            }

            if (args[1] == "ON")
            {
                m_debugMode = true;
                TerminalOut("Debug mode is now enabled.\n");
            }
            else if (args[1] == "OFF")
            {
                m_debugMode = false;
                TerminalOut("Debug mode is now disabled.\n");
            }
            else
            {
                TerminalOut("ERROR: The .DEBUG command takes a single argument, ON or OFF.\n");
            }
        }

        private void SetTimestampMode()
        {
            string[] args;

            args = m_currentCommand.Split(' ');

            if (args.GetUpperBound(0) != 1)
            {
                TerminalOut("ERROR: The .TIME command takes a single argument, ON or OFF.\n");
                return;
            }

            if (args[1] == "ON")
            {
                m_timestampMode = true;
                TerminalOut("Timestamp mode is now enabled.\n");
            }
            else if (args[1] == "OFF")
            {
                m_timestampMode = false;
                TerminalOut("Timestamp mode is now disabled.\n");
            }
            else
            {
                TerminalOut("ERROR: The .TIME command takes a single argument, ON or OFF.\n");
            }
        }

        private void SetLogMode()
        {
            string[] args;

            args = m_currentCommand.Split(' ');

            if (((args.GetUpperBound(0) == 1) && (args[1] != "OFF")) ||
                ((args.GetUpperBound(0) == 2) && (args[1] != "ON")))
            {
                TerminalOut("ERROR: The .LOG command expects either [OFF] or [ON filename] as arguments.\n");
                return;
            }

            if (args[1] == "ON")
            {
                m_sessionLogMode = true;
                SessionLogFilename = args[2];

                if (m_sessionLogMode == true)
                {
                    TerminalOut("Session logging is now enabled.\n");
                }
            }
            else if (args[1] == "OFF")
            {
                FlushLogBuffers();
                m_sessionLogMode = false;
                TerminalOut("Session logging is now disabled.\n");
            }
            else
            {
                TerminalOut("ERROR: The .LOG command expects either [OFF] or [ON filename] as arguments.\n");
            }
        }

        private void SetRFDataMode()
        {
            string[] args;

            args = m_currentCommand.Split(' ');

            if (((args.GetUpperBound(0) == 1) && (args[1] != "OFF")) ||
                ((args.GetUpperBound(0) == 2) && (args[1] != "ON")))
            {
                TerminalOut("ERROR: The .RFDATA command expects either [OFF] or [ON filename] as arguments.\n");
                return;
            }

            if (args[1] == "ON")
            {
                m_rfDataLogMode = true;
                RfDataLogFilename = args[2];

                if (m_rfDataLogMode == true)
                {
                    TerminalOut("RF data logging is now enabled.\n");
                }
            }
            else if (args[1] == "OFF")
            {
                FlushLogBuffers();
                m_rfDataLogMode = false;
                TerminalOut("RF data logging is now disabled.\n");
            }
            else
            {
                TerminalOut("ERROR: The .RFDATA command expects either [OFF] or [ON filename] as arguments.\n");
            }
        }

        private void RunScript()
        {
            string[] args;
            StreamReader scriptStreamReader;

            args = m_currentCommand.Split(' ');

            // Check that the command contains only two tokens: [.RUN] [filename]
            if (args.GetUpperBound(0) != 1)
            {
                TerminalOut("ERROR: The .RUN command takes a single argument, the filename.\n");
                return;
            }

            try
            {
                scriptStreamReader = new StreamReader(args[1]);
            }
            catch (Exception exc)
            {
                TerminalOut("ERROR: Could not open the script file '" + args[1] + "'\n");
                TerminalOut("CAUSE: " + exc.Message + "\n");
                return;
            }

            while (scriptStreamReader.EndOfStream != true)
            {
                string nextCommand;
                
                nextCommand = scriptStreamReader.ReadLine();
                nextCommand = nextCommand.Trim();

                if (nextCommand != "")
                {
                    m_scriptCommandQueue.Enqueue(nextCommand);
                }
            }
            
            scriptStreamReader.Close();

            m_scriptRunning = true;
        }

        private void DisplayCommandList()
        {
            TerminalOut("For more information on a specific command, type '? [command-name]'\n");
            TerminalOut(m_protocol.GetCommandList());
        }

        private void DisplayCommandUsage(string command)
        {
            string usage;

            try
            {
                usage = m_protocol.GetCommandUsage(command);
            }
            catch (ProtocolCommandParserError exc)
            {
                TerminalOut(exc.Message);
                return;
            }

            TerminalOut(usage);
        }

        private void ProcessCommand()
        {
            Packet txPacket = new Packet();

            if (m_currentCommand == "")
            {
                return;
            }

            try
            {
                txPacket = m_protocol.CommandToPacket(m_currentCommand);
                TransmitPacket(txPacket);
            }
            catch (ProtocolCommandParserError exc)
            {
                TerminalOut("ERROR: Unable to process command.\n");
                TerminalOut("CAUSE: " + exc.Message + "\n");
            }
            catch (FormatException exc)
            {
                TerminalOut("ERROR: Unable to process command. (Text when number expected?)\n");
                TerminalOut("CAUSE: " + exc.Message + "\n");
            }
        }

        public void TerminalOut(string output)
        {
            System.Console.Write(output);

            if (m_sessionLogMode == true)
            {
                LogOut(output);
            }
        }

        public void LogOut(string output)
        {
            output = output.Replace("\n", "\r\n");
            m_sessionLogStreamWriter.Write(output);
        }

        private void TransmitPacket(Packet txPacket)
        {
            byte[] txByteArray;

            if (m_sequence == 0xFF)
            {
                m_sequence = 0;
            }
            else
            {
                ++m_sequence;
            }
            txPacket.Sequence = m_sequence;

            if (m_debugMode == true)
            {
                PrintDebugPacket(txPacket);
            }

            txByteArray = txPacket.GetMessageByteArray();

            m_serialPort.Write(txByteArray, 0, txByteArray.GetUpperBound(0) + 1);
        }

        private void PrintDebugPacket(Packet debugPacket)
        {
            string debug = "";

            if (debugPacket.MessageType < 0x080)
            {
                debug += " <REQUEST>\n";
            }
            else
            {
                debug += " <RESPONSE>\n";
            }

            debug += "  H:";
            debug += " " + ByteToString(Packet.SYNCH_BYTE);
            debug += " " + ByteToString(debugPacket.NumEntries);
            debug += " " + ByteToString(debugPacket.BytesPerEntry);
            debug += " " + ByteToString(debugPacket.MessageType);
            debug += "\n";

            debug += "  P:";
            if (debugPacket.NumEntries == 0)
            {
                debug += " (empty)\n";
            }
            else
            {
                for (byte entryIndex = 0; entryIndex < debugPacket.NumEntries; ++entryIndex)
                {
                    byte[] byteArray = debugPacket.GetEntry(entryIndex);

                    if (entryIndex != 0)
                    {
                        debug += "    ";
                    }

                    for (int byteIndex = 0; byteIndex <= byteArray.GetUpperBound(0); ++byteIndex)
                    {
                        debug += " " + ByteToString(byteArray[byteIndex]);
                    }

                    debug += "\n";                    
                }
            }

            debug += "  F:";
            debug += " " + ByteToString(debugPacket.CalculateChecksum());
            debug += " " + ByteToString(debugPacket.Sequence);
            debug += "\n";

            if (debugPacket.MessageType < 0x080)
            {
                debug += " </REQUEST>\n";
            }
            else
            {
                debug += " </RESPONSE>\n";
            }

            TerminalOut(debug);
        }

        static public string ByteToString(byte printByte)
        {
            return String.Format("{0:X2}", printByte);
        }
    }
}
